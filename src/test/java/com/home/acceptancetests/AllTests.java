package com.home.acceptancetests;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"com.home"},
        features = ".",
        plugin = {"pretty"}
//        tags = {"@complete"}
)
public class AllTests {
}