package com.home.acceptancetests;

import javax.inject.Inject;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Hooks {

    Logger LOGGER = LoggerFactory.getLogger(Hooks.class);
    private final WebDriver driver;

    @Inject
    public Hooks(final WebDriver driver) {
        this.driver = driver;
    }

    @Before
    public void openWebSite(Scenario scenario) {
        LOGGER.info("\n===============================\n"
                +"SCENARIO STARTED\n"
                +scenario.getName()
                +"\n===============================\n");
        driver.navigate().to("http://www.google.co.uk");
    }

    @After
    public void closeSession(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver)
                    .getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
            LOGGER.info("\n===============================\n"
                    +"SCENARIO FAILED!\n"
                    +scenario.getName()
                    +"\n===============================\n");//stick it in the report
        }else {
            LOGGER.info("\n===============================\n"
                    + "SCENARIO PASSED!\n"
                    + scenario.getName()
                    + "\n===============================\n");
        }
        driver.quit();
    }


}
