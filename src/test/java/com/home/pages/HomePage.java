package com.home.pages;


import com.google.inject.Key;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.annotations.Step;

import javax.inject.Inject;


public class HomePage {

    private WebDriver webDriver;

    @FindBy(id = "lst-ib")
    private WebElement findField;

    @Step
    public void fillFindField(String value){
        findField.sendKeys(value);
    }


    @Inject
    public HomePage(final WebDriver webDriver){
        PageFactory.initElements(webDriver, this);
        this.webDriver = webDriver;
    }

}
