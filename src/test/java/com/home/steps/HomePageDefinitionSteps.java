package com.home.steps;

import javax.inject.Inject;

import com.home.pages.HomePage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.Given;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.*;

public class HomePageDefinitionSteps extends CommonDefinitionSteps {

    private Logger LOGGER = LoggerFactory.getLogger(HomePageDefinitionSteps.class);

    @Inject
    private HomePage homePage;

    @Given("^I search for \"([^\"]*)\"$")
    public void i_search_for(String arg1) {

        homePage.fillFindField(arg1);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Then("^I google should show me results!$")
    public void i_google_should_show_me_results() {
        assertThat(false).isEqualTo(true);
    }
}